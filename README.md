## Installation:
`npm i`

## Run the program: 
`npm start`

## Root Router: 
`/api/taskservice`

## Endpoints: 

### Check-in:
`POST /api/taskservice/checkin` - create a new checkin event/new user.

### Check-out: 
`PUT /api/taskservice/checkout/:user` - create a new checkout event.

### Report: 
`GET /api/taskservice/report` - returns all the tasks of the users.

Author: Ron Nabet