export interface IUser {
    user: String,
    tasks: any[],
    checkoutStatus: Boolean
}

export interface taskObj {
    task: String,
    startTime: String,
    endTime: String,
    time: String
}

