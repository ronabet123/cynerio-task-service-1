import {IUser, taskObj} from './task.interface';
import { UserModel } from './task.model';

export class TaskRepository {
    static createCheckIn(newCheckIn: IUser) {
        return UserModel.create(newCheckIn);
    }

    static updateUser(user: any) {
      return UserModel.findOneAndUpdate({user: user.user}, user, {useFindAndModify: false});
    }

    static updateTasksArr(user: String, task: taskObj) {
        return UserModel.findOneAndUpdate({user: user}, { $push: { tasks: task  }, checkoutStatus: false }, {useFindAndModify: false}).exec();
    }

    static getAllUsersTasks() {
        return UserModel.find({});
    }
    
    static findUser(user: String) {
        return UserModel.findOne({user: user}, {useFindAndModify: false});
    }
}

